# Try Hack Me Notes
As of 09/07/22 following Pre Security Path, can be found at https://tryhackme.com/paths then choosing Pre Security


## Open Systems Interconnection (OSI) Model

Layer  | Name | Notes
------:|:----- :|----
1      | Physical | Bits-electrical impulses, laser pulses in fibre optic etc
2      | Data Link | Frames, handles physical addresseing i.e. MAC Adresses
3      | Network | Packets, handles logical addressinng i.e. IP
4      | Transport | Segments/Datagrams, TCP, UDP etc operate at this level
5      | Session |
6      | Presentation | Translates data to and from form useful to application
7      | Application

## Transmission Control Protocol (TCP)

### TCP Layers

Layer   | Name | OSI equivalent
------: | :--- |:---
1       | Network Interface | 1 & 2
2       | Internet | 3
3       | Transport | 4
4       | Application | 5-7

Layer 1 may be split into Data Link & Physical layers as per the OSI, this is recognised but not as specified by [RFC 1122](https://datatracker.ietf.org/doc/html/rfc1122)

### TCP Commands
1. SYN - Client tries to initiate session
2. SYN/ACK - Server acknowledges
3. ACK - Client acknowledges data received successfully
4. DATA - Server sends data. Will keep repeating until Client stops sending ACKs and then
5. FIN - Client requests end of session
6. FIN/ACK - Server acknowledges
7. RST - Client abruptly ends session

First 3 commands are the 3-way handshake. This glosses other the details.
Connection is over a **socket** i.e. an IP address/port combo

## Domain Name Servers (DNS)

### Domain Names
NB. Top of the hierarchy is literally the . before .com, .gov etc. A TLD is a Top Level Domain. A gTLD is a *generic* TLD e.g. .com, .gov. A second-level domain name is directly below a tld e.g. bbc.com (but NOT bbc.co.uk as col.uk as a second level domain. Ordinal free term for generally available suffix is public suffix domain). A domain name has a maximum of 63 characters, all a-z, 0-9 or hyphen. Domain names cannot start or end with hyphens or have consecutive hyphens.
DNS resolution typically performed by a recursive DNS query which first checks hosts file, then local DNS cache, then a recursive DNS Server.

The recursive DNS server delegates the search recursively to:

- Recursive DNS Server
- Root name server
- TLD (e.g. com, org) authoratitive server
- domain name (e.g. sohohousing.org.uk) authorative server

All levels cache results so do not need to query entire hierarchy for all queries. Cache results have Time To Live (TTL) in seconds, if this has expired cache is invalidated and higher level queried.

NB caching makes servers vulnerable to cache poisoning.

Can carry out an iterative search where recursive server climbs the hierachy itself rather thna delegating to next level above but this is uncommmon.

See [NSLookup.io](https://www.nslookup.io/learning/recursive-vs-authoritative-dns/) for more 

### Subdomains
subdomains. e.g. thistledown.deviantart.com have the same restriction re characters as above. The overall name cannot be longer than 253 characters.

### Record Types
A-IPv4
AAAA-IPv6
CNAME-redirects to another domain (cannot map to IP address).
MX-mail server
TXT-general purpose


## Ports
Range is 0-1024
[List of Ports](http://www.vmaxx.net/techinfo/ports.htm)

## Switches & Routers
Routers can connect to internet and work at Layer 1 (Data Link) or Layer 3 (Transport). A Layer 2 Switch cannot work at Layer 3, so a Router is a device that can work at Layer 3. Layer 2 devices handle *frames* using *MAC addresses* and Layer 3 devices handle *packets* using *IP addresses*

## Firewalls
### Stateless-Layer 3
Rules apply to individual packets only.

### Stateful-Layer 4
Keep track of connections. Will block devices that try to re-establish bad connections
Layer 7 also exist, will make decision based on application

### Web Application Firewall e.g. Cloudflare
Sits in front of app, defends from DOS, cross-site scripting etc

[More details on Firewalls](https://www.swhosting.com/en/comunidad/manual/firewall-perimetral-de-capa-4-gestionable)

## Web Server
Software that serves files from root directory, standardised by the various servers
e.g. http://www.example.com/picture.jpg served from /var/www/html/picture.jgp in Apache or Nginx on Linux

### Virtual Host
Text config file rather than actual computer. Config files map directories to the virtual host's root directories.

### Backend Code
Typically not visible to client

## Load Balancer
Various algorithms e.g. Round Robin
Performs health check on servers periodically

## Virtual Private Networks (VPNs)
Establish encrypted tunnel through public network
Point To Point Protocol (PPTP) uses IPSec to encrypt the data into PPP packets.
As uses standard TCP/IP stack is widely supported (NB Wikipedia advises PPTP is obsolete)

## Content Delivery Networks (CDNs) e.g. Cloudflar
Store assets (images, html etc) in different locations so when request comes in closest one can be used. *Caches content at the network edge*.

## Uniform Resource Locators (URLs)

scheme: http/https:
user: user.password@
host/domain: tryhackme.com
port (optional): 80
/path: view-room
query-string: ?id=1
fragment: # task 3 (often refers to named anchor on page)

## HTTP Request
GET/HTTP/1.1
Host: tryhackme.com
User-Agent:
Referer:

(other header fields possible)
Always end with a blank line so web server knows request is finished.

### HTTP Response Codes
Full list at [Mozilla](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status) formal defined by [RFC 9110](https://httpwg.org/specs/rfc9110.html#overview.of.status.codes)
Grouped as follows:

- 100-199: Information, not commonly used
- 200-299: Success
- 200-399: Redirection
- 400-499: Client Error
- 500-599: Server Error

Common used Codes are:
- 200 OK
- 201 Created
- 301 Permanent Redirect
- 302 Temporary Redirect
- 400 Bad Request
- 401 Unauthorized (actualy means client is unauthenticated)
- 403 Forbidden (NB may issue 404 instead if wants to hide resource)
- 404 Not Found
- 500 Internal Server Error
- 503 Service Unavailable


## Unix
### Shell Operators
& run command in background
&& run multiple commands e.g. command1 && command2 NB command 2 etc only executes if command1 completes successfully
\> redirect output to file (overwrite)
\>> redirect output to file (append)

### Cron jobs
Use cron to schedule regular jobs, exactly when set using a file called a crontab.
A crontab is simply a special file with formatting that is recognised by the cron process to execute each line step-by-step. Crontabs require 6 specific values:

Value	Description
MIN	What minute to execute at
HOUR	What hour to execute at
DOM	What day of the month to execute at
MON	What month of the year to execute at
DOW	What day of the week to execute at
CMD	The actual command that will be executed.

Let's use the example of backing up files. You may wish to backup "cmnatic"'s  "Documents" every 12 hours. We would use the following formatting: 

0 *12 * * * cp -R /home/cmnatic/Documents /var/backups/	

This can be confusing to begin with, which is why there are some great resources such as the online [Crontab Generator](https://crontab-generator.org/) that allows you to use a friendly application to generate your formatting for you! As well as the site [Cron Guru](https://crontab.guru/)!

The cron service (daemon) runs in the background and constantly checks the /etc/crontab file, and /etc/cron.*/ directories. It also checks the /var/spool/cron/ directory.

Can create or edit a crontab file by running crontab -e which opens in the default text editor

### Package managers
Linux uses apt
MacOS uses Homebrew
above not exhaustive	

## Windows

Windows files are saved in the directory saved in the environment variable %windir%, which is typically C:/Windows
System 32 contains files crucial to the operating system
Windows-can access Local Users & Groups management centre by entering lusrmgr in search box

### Living off the Land

Attackers use built-in Windows tools & utlities to avoid detection. They may replace utilties with compromised versions. [Link}(https://lolbas-project.github.io/)

### Anti Malware Scan interface

An API that can be called by any program, so a program that executes a script could call AMSI to check the script. More at [AMSI](https://learn.microsoft.com/en-us/windows/win32/amsi/how-amsi-helps)


### Credential Guard

Protects credentials including password hashes and Kerberos Ticket Granting Tickets from unauthorised access, stops attackers hijacking them in pass the hask or pass the ticket attacks. More at [Credential Guard Overview](https://learn.microsoft.com/en-us/windows/security/identity-protection/credential-guard/)

### Updates

Since Windows 10 cannot indefinitely postpose updates, will be forced restart at some point.
 
### Firmware

UEFI replacing BIOS as firmware from about 2000, UEFI has GUI, BIOS strictly keyboard. 

From    https://www.techtarget.com/whatis/definition/Unified-Extensible-Firmware-Interface-UEFI#:~:text=Unified%20Extensible%20Firmware%20Interface%20(UEFI)%20is%20a%20specification%20for%20a,its%20operating%20system%20(OS).
 
“UEFI functions via special firmware installed on a computer's motherboard. Like BIOS, UEFI is installed at the time of manufacturing and is the first program that runs when booting a computer. It checks to see which hardware components are attached, wakes up the components and hands them over to the OS. The new specification addresses several limitations of BIOS, including restrictions on hard disk partition size and the amount of time BIOS takes to perform its tasks.”

### Bitlocker
Encrypts disks. Ideally uses Trusted Platform Module, chip on system that stores keys (TPM). Bitlocker needs TPM verision 1.2 or later. Without this need a USB start-up key. Encryption is AES 128-bit by default. Key is backed up to Entra ID or Microsoft account if no Entra ID account.

### Volume Shadow Copy Service (VSS)
Creates snapshots & restore points. If enabled can:
- create restore point
- perfornm system restore
- configure restore settings (e.g. which volumes to restore).
- delete restore points.

VSS is  a target for malware as it can be used to unwind a ransomware attack, so attackers may try to disable it to prevent this.


### CMD.exe
Can launch from System Configuration (Run msconfig) but obviously just direct by Run cmd.
Can see help on command by suffixing with /? e.g. ipconfig/? OR if command takes sub-commands suffix with help e.g. for net command (net share, net localgroup etc) use net help 

Useful commands:

- ipconfig espec ipconfig/all
- netstat-gives network info including connected ports etc
- whoami
- hostname

### UAC (User Access Control)
By default users do not run with admin permission, instead are prompted when admin permissions needed. 

### Task Manager
[Great reference at How To Geek](https://www.howtogeek.com/405806/windows-task-manager-the-complete-guide/)
Brief takeaways:
Can launch with Ctrl+Shift+Esc

Right click on processes includes:
- Switch To (Opens program window)
- End Task
- Run New Task
- Always on Top
- Open File Location
- Properties (Opens Properties for program's .exe file)	

Tabs:
- Performance (includes IP address & CPU & GPU models, system uptime, disk make & model, network adapter details
- App History
- Startup-can stop programs running at Startup, right-click gives lots of useful info
- Users

Can restart Windows Explorer from Task Manager by right-clicking

Can even get more powerful version of Task Manager, Process Manager, which is a free download from Microsoft

### System Configuration
Need local admin rights to open.
Can launch from start menu by searching for msconfig or run msconfig.exe from cmd window or Powershell.	
Lots of info [here](https://learn.microsoft.com/en-us/troubleshoot/windows-client/performance/system-configuration-utility-troubleshoot-configuration-errors
)	

Various utilities can be launched from System Configuration:
- MSInfo tool give details on Hardware Resources, Components, Software Environment, inclues list of Environment Variables
- Resource Monitor which is like Task Manager but more info, can use do find which processes have which file handles to identify conflicts and resolve them without closing apps, forcing users off etc
-Cmd.exe but see at top of page as has own entry

#### Registry Editor 
To edit Registry, which is a database of settings for:
- User Profiles
- Application information including document types
- Property sheets for folders & application icons
- hardware
- ports in use

Registry keys stored directly in Registry unless over 2048 bytes, then stored as files and filenames stored in Registry. Supporting files stored in 
%SystemRoot%\System32\Config except those for HKEY\_CURRENT\_USER which are stored in %System32%\Profiles\Username . NB max size of 64K for Registry key.

Should back up Registry before editing. This is done by default when backing up System State and stored in %SystemRoot%\Repair

Various programmatic ways to edit Registry.

[More info on this page](https://learn.microsoft.com/en-us/troubleshoot/windows-server/performance/windows-registry-advanced-users)		


#### StartUp Options	
Can select which services run at start up to help diagnose problems. NB for diagnosis only,to manage start programs should use Task Manger or another utility. Options are:
- Diagnostic Setup-disables networking, logging and lots of other stuff
- Selective Setup- can descide what starts

#### Control Panel
Can launch as control.exe

#### UAC Settings
Can set level where Windows alerts you to changes, all the way from all changes including those made by yourself to no alerts at all (not recommended)

### Computer Management
Groups together lots of utilities for managing the computer. Can be launched as  ** compmgmt.msc ** . [Full list of .msc utilities](https://www.groovypost.com/howto/windows-10-keyboard-shortcuts/) Has 3 sections: System Tools, Storage, Services & Applications

#### System Tools

##### Task Scheduler

##### Event Viewer
Used to view events & logs. 

###### Events
5 types of events:
- Error
- Warning
- Information
- Success Audit
- Failure Audit
More detail at (https://docs.microsoft.com/en-us/windows/win32/eventlog/event-types)

###### Logs
5 types of Logs
- Application
- Security
- System
- CustomLog
More detail at (https://docs.microsoft.com/en-us/windows/win32/eventlog/eventlog-key)

##### Shared Folders
Inclues: 
- Shares
- Sessions
- Open Files - Useful for getting handles of files if have a file you cannot close to see who has it open.

##### Local Users & Groups
can be opened directly by invoking lusrmgr.msc

##### Performance
Opens Performance Monitor which can be opened directly with perfmon.msc. Monitors performance real-time or from log.

##### Device Manager

#### Storage 
Includes Disk Management

#### Services & Applications
NB a Service is a daemon, an application that runs in the background	
	
	
## Offensive Security		

### Gobuster
Command line utility to carry out brute force scans for hidden webpages, checking if pages match wordlist (content? Name? both?)

## Defensive Security

Importance of documeting assets so you know what to maintain
Cyber Security Awareness also important

### Security Operations Centre (SOC)
Team of professionals monitoring environment for threats
Areas of interest are:
- Vulnerabilities
- Policy Violations
- Unauthorised Activity
- Network Intrusions

## Activities:
- Find Vulnerabilities
- Detect Unauthorised Activity
- Discover Policy Violations
- Detect Intrusions
- Support Incident Responses
- Monitor Security Posture
- Analyse Malware
- Report

## Proactive Services
- Network Security Monitoring
- Threat Hunting
- Threat Intelligence

### Threat Intelligence
Be aware of likely threats so defense is threat-informed.

### Digital Forensics and Incident Response
- Digital Forensics
- Incident Response
- Malware Analysis


Malicious IPs can be reported on AbuseIPDB.

### Identification & Authentication
Indentification-identifying unique individual
Authentication-checking they are who they say they are
Vulnerabilities:
- Allowing brute force
- Storing credentials in plain text
- Allowing weak passwords.

### Broken Access Control
e.g. Not applying principle of least privilege.
User can see another's user's records
Seeing a record by infering the value of a unique identifier e.g Insecure Direct Object References (IDORs) on website, put in URL ending in image71, product55 etc
Allowing unauthorised access to resources that should need authentication e.g. user settings pages.
Injection-not sanitising input.

### Host Firewalls 
Software soluton on a host blocking inbound & outbound connnections e.g. Windows Defender Firewall

## Digital Forensics

### Cyber Kill Chain
Series of steps that a Cyber Attack takes:

- Recon
- Weaponisation
- Delivery
- Exploitation
- Installation
- Command & Control
- Action on Objectives

### Miscellaneous
EXIF standard for saving metadata for image files. Tools to extract exif data e.g. exiftool

